//package carreras;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Carrera {
	
	ArrayList<Naves> nave = new ArrayList();


	apuesta apuesta1 = new apuesta();
	random random1 = new random();
	Estanque estanque1 = new Estanque();
	turbinas turbinas1 = new turbinas();
	alas alas1 = new alas();
	
	double Pista = 58;

	int Uapuesta;
	int contador = 0;
	
	
	Carrera(){


	Uapuesta = apuesta1.recibirApuesta();
	hacerNaves();

	while(contador<3) {
		Fallos();
		Efectos();
		contador = controladorderecorrido();
		System.out.println("CONTADOR: "+contador);
		muestraMiembros();
		try {
            Thread.sleep(6000);
         } catch (Exception e) {
            System.out.println(e);
         }
		
	}
		
	System.out.println("apuesta: "+Uapuesta);
	
	}
	
		
	public void hacerNaves() {
		String nombre = null;
		String combustible = null;
		int auxRandom;
		
		for (int i=0; i<3;i++) {
			if(i==0) {
				nombre = "mario";
			}
			else if(i==1) {
				nombre = "warrio";
			}
			else if(i==2) {
				nombre = "yoshi";
			}
			Naves nave1 = new Naves();
			auxRandom = random1.Nrandom(1);
			combustible = estanque1.azarCombustible(auxRandom);
			nave1.setNombre(nombre);
			nave1.setCombustible(combustible);
			nave1.setVelocidad(20);
			nave1.setTurbinas(4);
			nave1.setRecorrido(0);
			nave1.setPuesto(0);
			nave1.setAlas(2);
			agregaMiembro(nave1);
			
			
		}
	}
	
	public void Fallos() {
		int auxRandom=0;
		int cantidadTur;
		int cantidadAlas;
		
		
		for (Naves i: nave) {
			//fallo turbinas
			auxRandom = random1.Nrandom(2);
			
			if(auxRandom == 1 && i.getPuesto() == 0) {
				cantidadTur = i.getTurbinas();
				System.out.println(i.getNombre()+ " Se le ha roto una turbina");
				cantidadTur = cantidadTur - 1;
				i.setTurbinas(cantidadTur);
			}
			//fallo alas
			auxRandom = random1.Nrandom(2);
			
			if(auxRandom == 1 && i.getPuesto() == 0) {
				cantidadAlas = i.getAlas();
				System.out.println(i.getNombre()+ " Se le ha roto un Ala");
				cantidadAlas = cantidadAlas - 1;
				i.setAlas(cantidadAlas);
			}
		}
		
	}
	
	public void Efectos() {
		double velocidad;
		int cantidadTur;
		double efectoTur;
		int cantidadAlas;
		double efectoAlas;
		String combustible=null;
		double efectoCombustible;
		
		for (Naves i: nave) {
			//efecto turbina
			if(i.getRecorrido()<= Pista && i.getPuesto() == 0) {
				cantidadTur = i.getTurbinas();
				velocidad = i.getVelocidad();
				efectoTur = turbinas1.Efecto(cantidadTur);
				
				velocidad = velocidad * efectoTur;
				i.setVelocidad(velocidad);
				}
			//efecto alas
			if(i.getRecorrido()<= Pista && i.getPuesto() == 0) {
				cantidadAlas = i.getAlas();
				velocidad = i.getVelocidad();
				efectoAlas = alas1.Efecto(cantidadAlas);
				
				velocidad = velocidad * efectoAlas;
				i.setVelocidad(velocidad);
				}
			//bonus
			if(i.getVelocidad()<=5 && i.getPuesto() == 0) {
				
				velocidad = i.getVelocidad();
				velocidad = velocidad + 10;
				i.setVelocidad(velocidad);
				System.out.println(i.getNombre()+" este ql recibio bonus");
				
			}
			
			
			if(i.getRecorrido()<= Pista && i.getPuesto() == 0) {
				combustible = i.getCombustible();
				velocidad = i.getVelocidad();
				efectoCombustible = estanque1.Efecto(combustible);
				
				velocidad = velocidad * efectoCombustible;
				i.setVelocidad(velocidad);
				}
		}		
	}
	
	
		
	//agregar naves al arreglo 
	public void agregaMiembro(Naves nave1) {
			
			nave.add(nave1);
			
		}
	
	

	public void muestraMiembros() {
		
		System.out.println("-------------------------------------------------------------------------------------------------------------------------------");

		
		for (Naves i: nave) {
			if(i.getRecorrido() <= Pista) {
				System.out.println("Nombre: "+i.getNombre());
				System.out.println("Combustible: "+i.getCombustible());
				System.out.println("Veloc. Actual: "+i.getVelocidad());
				System.out.println("Tur: "+i.getTurbinas());
				System.out.println("Alas:" +i.getAlas());
				System.out.println("Recorrido: "+i.getRecorrido());
				System.out.println("-------------------------");
				}
		}
	}
	
	public int controladorderecorrido() {
		double velocidad;
		double recorrido;
		int puesto = 1;
		
		for (Naves i: nave) {
			if(i.getRecorrido() <= Pista) {
				recorrido=i.getRecorrido();
				velocidad=i.getVelocidad();
				recorrido=velocidad+recorrido;
				i.setRecorrido(recorrido);
			}
			if(i.getRecorrido() >= Pista && i.getPuesto() == 0) {
				
				i.setPuesto(puesto);
				puesto = puesto +1;
				contador=contador+1;
				System.out.println(i.getNombre()+" Ha llegado a la meta y con una velociada de" +i.getVelocidad()+ ",rec: " +i.getRecorrido()+"y lugar:"+ contador);
				
				
			}
		}
		return contador;
	}
}
	
