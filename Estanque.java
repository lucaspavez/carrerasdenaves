//package carreras;

public class Estanque {
	

	public String azarCombustible(int opcion){
		String combustible = null;	
		
		if(opcion==1) {
			combustible="espacial";
		}
		
		else if(opcion==2) {
			combustible="CataPower";
		}
		else if(opcion==3) {
			combustible="nuclear";
		}
		
		return combustible;
			
	}
	
	public double Efecto(String combustible){
		double Efecto=1;
		
		if(combustible == "espacial") {
			Efecto=1.25;
		}
		else if(combustible == "CataPower") {
			Efecto=1.5;
		}
		else if(combustible == "nuclear") {
			Efecto=1;
		}
		
		return Efecto;
		
	}
	
	
}
